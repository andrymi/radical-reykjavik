---
name: Q - félag hinsegin stúdenta/ Q - Queer Student Association
website: https://queer.is
scrape:
  source: facebook
  options:
    page_id: Qfelag
---
The Gay Student Association was founded on January 25 1999 after repeated regular meetings about the situation of homosexuality at the University of Iceland. Members were quickly over 150. Through the years the association has been more radical than other queer movements and thus driven queer politics in Iceland in many ways. It was in the forefront with accepting both bisexual and trans people into the association, and was the first to use the Icelandic term „hinsegin“ (lit. different, translated as queer) in it’s formal name to emphasise that it fought for the rights of all queer people. This happened in the year 2008 when the associations name was changed to Q. 
