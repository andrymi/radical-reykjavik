---
name: Slagtog
website: http://www.slagtog.org
scrape:
  source: facebook
  options:
    page_id: slagtog
---
Slagtog eru félagasamtök um femíníska sjálfsvörn (FSV).
Slagtog sér um fræðslu og æfingar í FSV, en hún skiptir í fjóra þætti: tilifinningalega, sálræna, munnlega og líkamlega.
