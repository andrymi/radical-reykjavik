---
name: Black Lives Matter Iceland
website: mailto:blacklivesmattericeland@gmail.com
scrape:
  source: facebook
  options:
    page_id: BLMIceland
---
We are an organization for learning, hearing, and the comprehension of Black pain. We want to help Black people, especially youth, find their, voice, purpose, and pride.

