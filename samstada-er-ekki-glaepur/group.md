---
name: Samstaða er ekki glæpur
website: https://www.adstandaupp.com
scrape:
  source: facebook
  options:
    page_id: nidur19
---
19 grein lögreglulaga kveður á um skilyrðislausa hlýðni við skipanir lögreglu. Um þessar mundir er verið að sækja fólk til saka fyrir að hlýða ekki skipunum lögreglu um að hætta friðsömum mótmælum í þágu jafnréttis. Refsingar eru fjársektir sem, að viðbættum lögfræðikostnaði, hlaupa á hundruð þúsunda í hverju máli. Lögsóknirnar eiga augljóslega að fæla fólk frá að nýta sér stjórnarskrárvarinn rétt sinn til mótmæla. Þær eru því alvarleg aðför að lýðræði á Íslandi. 
