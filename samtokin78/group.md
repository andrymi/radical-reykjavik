---
name: Samtökin `78
website: http://www.samtokin78.is/
scrape:
  source: facebook
  options:
    page_id: samtokin78
---
Samtökin '78 eru samtök hinsegin fólks á Íslandi, þ. á m. lesbía, homma, tvíkynhneigðra, eikynheigðra, pankynhneigðra, trans fólks og intersex fólks.

machine translation: 
Die Vereinigung '78 ist eine Vereinigung schwuler Menschen in Island, z. á m. Lesben, Schwule, Bisexuelle, Schwule, Bisexuelle, Transgender und Intersexuelle.
