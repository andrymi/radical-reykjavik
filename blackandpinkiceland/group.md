---
name: Black & Pink Iceland
website: mailto:blackandpinkiceland@riseup.net
scrape:
  source: facebook
  options:
    page_id: blackandpinkiceland
---
Radical queer collective based in Reykjavík, Iceland.
