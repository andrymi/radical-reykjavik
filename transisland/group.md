---
name: Trans Ísland
website: http://transisland.is
scrape:
  source: facebook
  options:
    page_id: transisland
---
Trans Ísland er félag trans fólks á Íslandi. Félagið er málsvari, stuðningsamtök og baráttu samtök fyrir réttindum trans fólks hérlendis.
twitter: twitter.com/Trans_Island