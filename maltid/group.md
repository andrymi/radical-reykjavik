---
name: MÁL/TÍÐ
website: https://maltid.org/
scrape:
  source: facebook
  options:
    page_id: maltid.institute
---
MÁL/TÍÐ focuses on projects that challenge our current food culture and explore plausible alternatives inspired by new technology, philosophy and human sciences.

We are interested in how design and art can be used to criticize, experiment and prototype alternative food systems.

This platform connects stakeholders of our food systems, creates exchanges, explores prospective collaborations while opening the discussion with a broader audience.

MÁL/TÍÐ is a professional kitchen that can be used by different actors to experiment with food stuff, develop products and concepts.

It is a venue for designers and artists working with food and eating.

At last, it is an international network to connect Icelandic projects with similar institutions around the world and welcome foreign artists, designers, chefs and scientists to exhibit and host workshops on their practices.

Through lectures and workshops our audience discover and learn how art/design and science merge in meaningful food projects. Using senses, such as taste and smell, we create new food narratives and bring lights on hot debatable topics such as food waste, climate change, feminism and globalization.
