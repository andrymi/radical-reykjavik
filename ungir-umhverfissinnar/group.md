---
name: Ungir umhverfissinnar
website: https://www.umhverfissinnar.is/
scrape:
  source: facebook
  options:
    page_id: umhverfissinnar
---
Ungir umhverfissinnar eru frjáls félagasamtök. Tilgangur félagsins er að vera vettvangur ungs fólks á Íslandi til að láta gott af sér leiða í umhverfismálum.
