---
name: Slagtog
website: http://www.slagtog.org
scrape:
  source: facebook
  options:
    page_id: slagtog
---
FLÆÐI uses empty spaces in Reykjavík to create an open art venue which serves as a platform for increased visibility of artists on the fringe within the Icelandic society.
