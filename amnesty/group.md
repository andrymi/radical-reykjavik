---
name: Íslandsdeild Amnesty International
website: https://amnesty.is
scrape:
  source: facebook
  options:
    page_id: amnestyiceland
---
Amnesty International er alþjóðleg hreyfing fólks sem berst fyrir því að allir fái að njóta alþjóðlegra viðurkenndra mannréttinda. Félagar AI sameinast í baráttu fyrir betri heim.
