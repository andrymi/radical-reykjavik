---
name: Handmótuð áhrif - 1600 niðurfelld nauðgunarmál
website: 1600nidurfelld@gmail.com
scrape:
  source: facebook
  options:
    page_id: 1600cases
---
Frá 2000-2020 hafa um 1600 naugunarkærur verið felldar niður. Markmið verkefnisins er að myndgera hverja kæru með leirstyttu og sýna þannig fjöldann sjónrænt. Smiðjur eru auglýstar hér og eru allir hjartanlega velkomnir.
