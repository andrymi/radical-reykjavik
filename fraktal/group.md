---
name: Fraktal Reykjavík
website: https://www.karolinafund.com/project/view/2714
scrape:
  source: facebook
  options:
    page_id: fraktalreykjavik
---
Democratic, cooperative work-space. Artist desk rentals. Soon-to-be vegan café. Venue for cultural, political and artistic events. Kitchen and print shop in planning.
