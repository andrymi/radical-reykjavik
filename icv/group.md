---
name: Iceland Conservation Volunteers
website: https://www.ust.is/english/volunteers/
scrape:
  source: facebook
  options:
    page_id: ICV.is
---
Environment Agency Volunteers (Sjálfboðaliðar Umhverfisstofnunar) assist in the practical management of national parks, nature reserves and other areas of outstanding natural beauty throughout Iceland.

Every year, we complete more than 100 projects in at least 30 different protected areas around Iceland. About 200 volunteers work in total ca. 500 weeks in nature conservation.

