---
name: Refugees in Iceland
website: https://www.facebook.com/refugeesiniceland
scrape:
  source: facebook
  options:
    page_id: refugeesiniceland
---
Refugees in Iceland is an informal group created in the beginning of 2019 when a group of people seeking asylum in Iceland got together to and organised in solidarity to fight for their right to stay in Iceland and live dignified lives. Five of the original demands are: 1. Stop all deportations. 2. Substantial reveiw for all and no more Dublin regulation. 3. Equal Access to healthcare. 4. The right to work while going through the asylum process. 5. Closing down of the Refugee camp in Ásbrú and other isolated places: End the social isolation.

