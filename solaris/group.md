---
name: Solaris hjálparsamtök
website: https://www.facebook.com/solarishjalparsamtok
scrape:
  source: facebook
  options:
    page_id: solarishjalparsamtok 
---
Markmið SOLARIS er að stuðla að því að hælisleitendur og flóttafólk á Íslandi búi við mannúð, mannlega reisn, mannréttindi og réttlæti á við aðra þegna landsins. Tilgangur Solaris er að bregðast við þeirri neyð sem hælisleitendur og flóttafólk býr við víða á Íslandi, m.a. í formi bágra aðstæðna og skorti á nauðsynjum, að þrýsta á breytingar og umbætur í málefnum hælisleitenda og flóttafólks í landinu og að berjast fyrir bættri stöðu þeirra, réttlátri málsmeðferð, auknum réttindum og betra aðgengi, m.a. að nauðsynlegri þjónustu. Það hyggst SOLARIS gera í samstarfi við almenning, yfirvöld og önnur samtök sem vinna að sama markmiði.

SOLARIS is an aid organisation for refugees and asylum seekers in Iceland. The purpose of SOLARIS is to respond to the emergency of refugees and asylum seekers in Iceland who live in poor conditions in many residence centres; to push for changes in law, policy and actions when it comes to refugees and asylum seekers; and to fight for improvements when it comes to access to necessary services, inclusion and the rights of refugees and asylum seekers. SOLARIS aims to do this in cooperation with the public, authorities and other organisations who work towards the same goals.
