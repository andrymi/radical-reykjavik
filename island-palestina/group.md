---
name: Félagið Ísland-Palestína
website: https://www.palestina.is/
scrape:
  source: facebook
  options:
    page_id: islandpalestina
---
Félagið Beitir sér fyrir því að Íslendingar leggi sitt af mörkum til þess að réttlát og friðsamleg lausn finnist á deilu Ísraela og Palestínumanna á grundvelli alþjóðaréttar, mannréttindasáttmála og ályktana Sameinuðu þjóðanna. 

Markmið félagsins eru:
-Að stuðla að jákvæðum viðhorfum meðal Íslendinga til ísraelsku og palestínsku þjóðanna og vinna gegn aðskilnaðarstefnu og hver kyns mismunun á grundvelli uppruna, ætternis eða trúarbragða.
-Að kynna baráttu Palestínumanna gegn hernámi og fyrir sjálfsákvörðunarrétti og rétti palestínskra flóttamanna til að hverfa aftur til síns heimalands.
-Að beita sér fyrir því, að Íslendingar leggi sitt af mörkum til þess að réttlát og friðsamleg lausn finnist á deilu Ísraela og Palestínumanna á grundvelli alþjóðaréttar og mannréttindasáttmála Sameinuðu þjóðanna og með tilliti til allra samþykkta Sameinuðu þjóðanna sem varða deiluna.
-Að vinna að því að ríkisstjórn Íslands veiti PLO, Frelsissamtökum Palestínumanna, fulla stjórnmálalega viðurkenningu sem réttmætum fulltrúa palestínsku þjóðarinnar og styðji í verki rétt Palestínumanna til að stofna lífvænlegt, sjálfstætt og fullvalda ríki.

English:
The Association Iceland-Palestine was founded on November 29th, 1987. The Association’s goals were from the beginning to encourage positive attitudes to the Israeli and Palestinian people and to work against all forms of apartheid in Palestine. The Association supports the Palestinian struggle against occupation and refugees’ right of return. On May 18th 1989 the Icelandic National Assembly passed a resulution that endorses all the Association’s major goals, Israel’s right to existance and Palestinians’ national rights.
