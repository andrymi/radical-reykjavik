---
name: Snarrótin
website: http://www.snarrotin.is
scrape:
  source: facebook
  options:
    page_id: Snarrotin
---
Snarrótin – Samtök um skaðaminnkun og mannréttindi er félag áhugamanna um opið samfélag, mannréttindi, upplýsingafrelsi og nýjar leiðir í fíknivörnum.
Snarrótin – Samtök um skaðaminnkun og mannéttindi er félag áhugamanna um opið samfélag, mannréttindi, upplýsingafrelsi og nýjar leiðir í fíknivörnum.

Markmiðum sínum hyggst félagið ná með fyrirlestrahaldi, námskeiðum, birtingu fræðsluefnis, upplýsingamiðlun og með samstarfi við önnur mannréttindasamtök.

Félagið er óháð stjórnmálaflokkum og hagsmunaaðilum.

Snarrótin stafar í náinni samvinnu við Hungarian Civil Liberties Union (HCLU), Open Society Foundation og Law Enforcement Against Prohibition (LEAP).

Snarrótin hyggst mynda tengsl við önnur alþjóðleg samtök sem hafa velferð og borgaraleg réttindi á stefnuskrá sinni.

Machine translation:
Snarrótin - Die Vereinigung für Schadensminderung und Menschenrechte ist eine Vereinigung von Enthusiasten für eine offene Gesellschaft, Menschenrechte, Informationsfreiheit und neue Wege der Drogenprävention.
Snarrótin - Die Vereinigung für Schadensminderung und Menschenrechte ist eine Vereinigung von Enthusiasten für eine offene Gesellschaft, Menschenrechte, Informationsfreiheit und neue Wege der Drogenprävention.

Der Verein beabsichtigt, seine Ziele durch Vorträge, Kurse, Veröffentlichung von Lehrmaterial, Verbreitung von Informationen und in Zusammenarbeit mit anderen Menschenrechtsorganisationen zu erreichen.

Der Verein ist unabhängig von politischen Parteien und Interessengruppen.

Die Hauptursache ist eine enge Zusammenarbeit mit der Ungarischen Union für bürgerliche Freiheiten (HCLU), der Open Society Foundation und der Strafverfolgung gegen das Verbot (LEAP).

Snarrótin beabsichtigt, Verbindungen zu anderen internationalen Organisationen herzustellen, die Wohlfahrt und Bürgerrechte auf ihrer Tagesordnung haben.
